# Pento tech challenge

Thanks for taking the time to do our tech challenge.

The challenge is to build a small full stack web app, that can help a freelancer track their time.

It should satisfy these user stories:

- As a user, I want to be able to start a time tracking session
- As a user, I want to be able to stop a time tracking session
- As a user, I want to be able to name my time tracking session
- As a user, I want to be able to save my time tracking session when I am done with it
- As a user, I want an overview of my sessions for the day, week and month
- As a user, I want to be able to close my browser and shut down my computer and still have my sessions visible to me when I power it up again.

# Implementation

There are two independent parts to this app;

- A golang [time-tracker/api](api/tracker/README.md)
- A react [time-tracker/ui](ui/tracker/README.md)

## Common Requirements

- docker >= 19.03.5
- docker-compose >= 1.23.2

## Running the app locally

  $ docker-compose up

Open your default browser at http://localhost:8000

### Smoketest - api/db/ui

As part of the docker-compose up experience, a container running a very simple smoke-test has been added. 
On up, it uses the host network, to verify that the UI can communicate with the API correctly. 

The container orchestration was also updated, to ensure the API waits for the DB before starting.

## Possible roadmaps

- add end to end testing leveragin the docker-compose setup
- add test coverage setup for the UI
- improve test coverage for the API
- get ui feedback: should one be able to delete sessions? at which point in the flow?
- split saga/reducer/action functions into a more organized fashion
