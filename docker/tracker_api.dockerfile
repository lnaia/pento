# This is a multi-stage Dockerfile and requires >= Docker 17.05
# https://docs.docker.com/engine/userguide/eng-image/multistage-build/
# FROM gobuffalo/buffalo:v0.15.2 as builder
FROM golang:1.13.4-stretch as builder

# prepare repo dir
RUN mkdir /app
WORKDIR /app
ADD . .

# set api cwd
WORKDIR /app/api/tracker
ENV GO111MODULES=on

# install app dependencies
RUN go get ./...

# install buffalo cli
RUN wget https://github.com/gobuffalo/buffalo/releases/download/v0.15.2/buffalo_0.15.2_Linux_x86_64.tar.gz
RUN tar xzvf buffalo_0.15.2_Linux_x86_64.tar.gz
RUN cp -rvf buffalo /usr/local/bin/buffalo

RUN buffalo build --static -o /bin/app

FROM alpine
RUN apk add --no-cache bash
RUN apk add --no-cache ca-certificates

WORKDIR /bin/

COPY --from=builder /bin/app .

# wait for DB to be ready
COPY --from=builder /app/wait-for .
RUN chmod 777 wait-for

# copy prod settings
# ADD .env .

# Uncomment to run the binary in "production" mode:
ENV GO_ENV=production

# Bind the app to 0.0.0.0 so it can be seen from outside the container
ENV ADDR=0.0.0.0

EXPOSE 3000
