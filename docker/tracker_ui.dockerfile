FROM node:13.2-alpine as builder

# prepare directories
RUN mkdir -p /app
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
ENV PATH /app/.yarn/.bin:$PATH

# add app and install and cache tmp dependencies
ADD . .
WORKDIR /app/ui/tracker
RUN yarn install --frozen-lockfile

# build the app for production run
RUN yarn build

# sample config taken from: https://github.com/docker-library/docs/tree/master/nginx
FROM nginx
COPY ui/tracker/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/ui/tracker/build /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
