const { openBrowser, text, goto, closeBrowser } = require('taiko');

(async () => {
  try {
    await openBrowser({ args: ['--no-sandbox'] })
    await goto("localhost:8000");
    const result = await text('There has been a loading error').exists();
    if (result) {
      console.error('smoketest failed');
      process.exit(1);
    }
    console.log('smoketest pass');
    await closeBrowser();
  } catch (error) {
    console.error(error);
  } finally {
    closeBrowser();
  }
})();
