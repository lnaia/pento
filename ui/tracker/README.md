# Time Tracker - UI

This UI was bootstrapped w/ [Create React App](https://github.com/facebook/create-react-app).

## Dev Requirements

- node: 13.2.x
- yarn: 1.17.x

## Available Scripts

In the project directory, you can run:

- `yarn start`
- `yarn lint`
- `yarn build`
- `yarn storybook`
- `yarn build-storybook`

## Storybook

You can check the storybook catalog here
https://lnaia.gitlab.io/pento

## Landing page

![Preview](.preview.png "Preview")
