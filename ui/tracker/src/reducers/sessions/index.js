import { combineReducers } from 'redux';
import { list } from './list';
import { meta } from './meta';

export const sessions = combineReducers({
  list,
  meta,
});
