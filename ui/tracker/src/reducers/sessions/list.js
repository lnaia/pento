import { createGenericApiReducer } from "utils";
import { ACTIONS } from "constants/actions";
import { ACTION_STATUS } from "constants/action-status";
import { bindActionPrefix } from "utils/generic-factories";
import { sessionMapper, listToObj } from "../data-transformers";

const { IN_PROGRESS, SUCCESS, FAILURE, NOT_STARTED } = ACTION_STATUS;

const { FETCH_SESSIONS, CREATE_SESSION, SAVE_SESSION, FETCH_SESSION } = ACTIONS;

const sessionInitialState = (state, payload) => {
  const session = sessionMapper(payload);

  return {
    ...state,
    payload: {
      ...state.payload,
      [payload.id]: {
        data: session,
        meta: { requestStatus: NOT_STARTED }
      }
    }
  };
};

const updateSessionMeta = ({ state, payload, meta }) => ({
  ...state,
  [payload.id]: {
    ...state[payload.id],
    meta
  }
});

export const list = (state, action) => {
  const reducer = createGenericApiReducer(FETCH_SESSIONS, listToObj);
  const newState = reducer(state, action);
  const { type, payload } = action;

  switch (type) {
    case `${bindActionPrefix(CREATE_SESSION, SUCCESS)}`: {
      return sessionInitialState(state, payload);
    }

    case `${bindActionPrefix(FETCH_SESSION, SUCCESS)}`: {
      return sessionInitialState(state, payload);
    }

    case `${bindActionPrefix(SAVE_SESSION, IN_PROGRESS)}`: {
      return updateSessionMeta({
        state,
        payload,
        meta: { requestStatus: IN_PROGRESS }
      });
    }

    case `${bindActionPrefix(SAVE_SESSION, SUCCESS)}`: {
      return updateSessionMeta({
        state,
        payload,
        meta: { requestStatus: SUCCESS }
      });
    }

    case `${bindActionPrefix(SAVE_SESSION, FAILURE)}`: {
      return updateSessionMeta({
        state,
        payload,
        meta: { requestStatus: FAILURE }
      });
    }

    default:
      return newState;
  }
};
