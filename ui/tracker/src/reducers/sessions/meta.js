import { ACTIONS } from 'constants/actions';
import { ACTION_STATUS } from 'constants/action-status';
import { SESSION_OVERVIEW } from 'constants/sessions';
import { bindActionPrefix } from 'utils/generic-factories';

const {
  CREATE_SESSION,
  ON_CHANGE_LIST_OVERVIEW,
  SHOW_CREATE_SESSION,
  HIDE_CREATE_SESSION,
} = ACTIONS;

export const metaInitialState = {
  showCreateSession: false,
  overview: SESSION_OVERVIEW.DAY,
  perPage: 2000,
};

export const meta = (state = metaInitialState, action) => {
  const { type } = action;
  switch (type) {
    case `${bindActionPrefix(CREATE_SESSION, ACTION_STATUS.SUCCESS)}`: {
      return {
        ...state,
        showCreateSession: false,
      };
    }

    case ON_CHANGE_LIST_OVERVIEW: {
      return {
        ...state,
        overview: action.payload,
      };
    }

    case SHOW_CREATE_SESSION: {
      return {
        ...state,
        showCreateSession: true,
      };
    }

    case HIDE_CREATE_SESSION: {
      return {
        ...state,
        showCreateSession: false,
      };
    }

    default:
      return state;
  }
};
