import { combineReducers } from 'redux';
import { app } from './app';
import { sessions } from './sessions';

export const rootReducer = combineReducers({
  app,
  sessions,
});
