import * as moment from 'moment';

export const sessionMapper = ({
  id,
  name,
  current_state: state,
  total_time: totalTime,
  started_at: startedAt,
  created_at: createdAt,
  updated_at: updatedAt,
}) => ({
  id,
  name,
  state,
  totalTime,
  startedAt: moment(startedAt),
  createdAt: moment(createdAt),
  updatedAt: moment(updatedAt),
});

export const listToObj = (list) =>
  list.reduce(
    (obj, data) => ({
      ...obj,
      [data.id]: { data: sessionMapper(data) },
    }),
    {},
  );
