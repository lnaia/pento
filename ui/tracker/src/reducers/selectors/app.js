import { createSelector } from 'reselect';

export const getApp = (state) => state.app;

export const selectApiEndpoint = createSelector(
  getApp,
  ({ apiEndpoint }) => apiEndpoint,
);
