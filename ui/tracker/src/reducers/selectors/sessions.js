import { createSelector } from 'reselect';

export const getSessions = (state) => state.sessions;

export const selectSessionsList = createSelector(
  getSessions,
  ({ list }) => list,
);

export const selectSessionsMeta = createSelector(
  getSessions,
  ({ meta }) => meta,
);

export const selectSessionsPayload = createSelector(
  selectSessionsList,
  ({ payload }) => payload,
);

export const selectSessionsRequestStatus = createSelector(
  selectSessionsList,
  ({ meta }) => meta.requestStatus,
);

export const selectSessionsOverview = createSelector(
  selectSessionsMeta,
  ({ overview }) => overview,
);

export const selectSessionsPerPage = createSelector(
  selectSessionsMeta,
  ({ perPage }) => perPage,
);

export const selectSessionsCreateSession = createSelector(
  selectSessionsMeta,
  ({ showCreateSession }) => showCreateSession,
);

export const selectSessionsParams = createSelector(
  selectSessionsOverview,
  selectSessionsPerPage,
  (overview, perPage) => ({ overview, perPage }),
);
