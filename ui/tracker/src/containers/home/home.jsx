import React from "react";
import { Container } from "@material-ui/core";
import { Sessions } from "components/sessions";
import { useStyles } from "./home.styles";

export const Home = () => {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Sessions />
    </Container>
  );
};
