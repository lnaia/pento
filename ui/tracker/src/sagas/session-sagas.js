import { select } from 'redux-saga/effects';
import {
  sessionActions,
  sessionsActions,
  saveSessionActions,
  stopSessionActions,
  startSessionActions,
  finishSessionActions,
  createSessionActions,
} from 'actions/sessions';
import { selectSessionsParams } from 'reducers/selectors/sessions';
import { genericSaga } from 'utils';
import {
  listSessions,
  singleSessionRequest,
  createSessionRequest,
  saveSessionRequest,
  startSessionActionRequest,
  stopSessionActionRequest,
  finishSessionActionRequest,
} from 'requests/sessions';

export function* getSessions() {
  const { overview, perPage } = yield select(selectSessionsParams);

  yield genericSaga({
    ...sessionsActions,
    params: { overview, perPage },
    request: listSessions,
  });
}

export function* createSession({ payload: { name } }) {
  yield genericSaga({
    ...createSessionActions,
    params: { name },
    request: createSessionRequest,
  });
}

export function* saveSessionData({ payload: { id, name } }) {
  yield genericSaga({
    ...saveSessionActions,
    params: { id, data: { name } },
    request: saveSessionRequest,
  });
}

export function* startSessionAction({ payload: { id } }) {
  yield genericSaga({
    ...startSessionActions,
    params: { id },
    request: startSessionActionRequest,
  });
}

export function* stopSessionAction({ payload: { id } }) {
  yield genericSaga({
    ...stopSessionActions,
    params: { id },
    request: stopSessionActionRequest,
  });
}

export function* finishSessionAction({ payload: { id } }) {
  yield genericSaga({
    ...finishSessionActions,
    params: { id },
    request: finishSessionActionRequest,
  });
}

export function* fetchSession({
  payload: { id },
  meta: {
    params: { id: idParams },
  },
}) {
  yield genericSaga({
    ...sessionActions,
    params: { id: id || idParams },
    request: singleSessionRequest,
  });
}
