import { takeLatest } from 'redux-saga/effects';
import { ACTIONS } from 'constants/actions';
import { ACTION_STATUS } from 'constants/action-status';
import { bindActionPrefix as bindAction } from 'utils/generic-factories';
import {
  getSessions,
  fetchSession,
  createSession,
  saveSessionData,
  startSessionAction,
  stopSessionAction,
  finishSessionAction,
} from './session-sagas';

const { SUCCESS } = ACTION_STATUS;

const {
  FETCH_SESSIONS,
  FETCH_SESSION,
  FINISH_SESSION,
  SAVE_SESSION,
  START_SESSION,
  STOP_SESSION,
  CREATE_SESSION,
  ON_CHANGE_LIST_OVERVIEW,
} = ACTIONS;

export function* watchSessionsRequest() {
  yield takeLatest(FETCH_SESSIONS, getSessions);
  yield takeLatest(ON_CHANGE_LIST_OVERVIEW, getSessions);
}

export function* watchCreateSessionsRequest() {
  yield takeLatest(CREATE_SESSION, createSession);
}

export function* watchSaveSessionRequest() {
  yield takeLatest(SAVE_SESSION, saveSessionData);
}

export function* watchStartSessionRequest() {
  yield takeLatest(START_SESSION, startSessionAction);
}

export function* watchStopSessionRequest() {
  yield takeLatest(STOP_SESSION, stopSessionAction);
}

export function* watchFinishSessionRequest() {
  yield takeLatest(FINISH_SESSION, finishSessionAction);
}

export function* watchFetchSessionRequest() {
  yield takeLatest(FETCH_SESSION, fetchSession);
  yield takeLatest(`${bindAction(SAVE_SESSION, SUCCESS)}`, fetchSession);
  yield takeLatest(`${bindAction(START_SESSION, SUCCESS)}`, fetchSession);
  yield takeLatest(`${bindAction(STOP_SESSION, SUCCESS)}`, fetchSession);
  yield takeLatest(`${bindAction(FINISH_SESSION, SUCCESS)}`, fetchSession);
}
