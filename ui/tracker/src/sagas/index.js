import { all, fork } from 'redux-saga/effects';
import {
  watchSessionsRequest,
  watchCreateSessionsRequest,
  watchSaveSessionRequest,
  watchStartSessionRequest,
  watchStopSessionRequest,
  watchFinishSessionRequest,
  watchFetchSessionRequest,
} from './session-watchers';

/*
    A few different options here for our rootSaga effect styles
    I've chosen for this implementation to go with nesting
    our forks inside an all effect - however I don't have much of
    a preference between this, the standard all effect alone (blocking)
    and simple forks
    https://redux-saga.js.org/docs/advanced/RootSaga.html
 */

export function* rootSaga() {
  yield all([
    fork(watchSessionsRequest),
    fork(watchCreateSessionsRequest),
    fork(watchSaveSessionRequest),
    fork(watchStartSessionRequest),
    fork(watchStopSessionRequest),
    fork(watchFinishSessionRequest),
    fork(watchFetchSessionRequest),
  ]);
}
