export const SESSION_STATE = {
  INITIAL: 'initialState',
  STARTED: 'started',
  STOPPED: 'stopped',
  FINISHED: 'finished',
};
export const SESSION_OVERVIEW = {
  ALL: 'all',
  DAY: 'day',
  WEEK: 'week',
  MONTH: 'month',
};
