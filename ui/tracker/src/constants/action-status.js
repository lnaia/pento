import keyMirror from 'keymirror';

export const ACTION_STATUS = {
  ...keyMirror({
    NOT_STARTED: null,
    IN_PROGRESS: null,
    SUCCESS: null,
    FAILURE: null,
  }),
};
