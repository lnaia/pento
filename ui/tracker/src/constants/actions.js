import keyMirror from 'keymirror';

export const ACTIONS = {
  ...keyMirror({
    FETCH_SESSION: null,
    FETCH_SESSIONS: null,
    CREATE_SESSION: null,
    SAVE_SESSION: null,
    START_SESSION: null,
    STOP_SESSION: null,
    FINISH_SESSION: null,
    ON_CHANGE_LIST_OVERVIEW: null,
    SHOW_CREATE_SESSION: null,
    HIDE_CREATE_SESSION: null,
  }),
};
