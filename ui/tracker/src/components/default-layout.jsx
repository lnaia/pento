import React from "react";
import * as PropTypes from "prop-types";
import { Header } from "components/header/header";
import { Route } from "react-router-dom";

export const DefaultLayout = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <>
        <Header />
        <Component {...matchProps} />
      </>
    )}
  />
);

DefaultLayout.propTypes = {
  component: PropTypes.any.isRequired // eslint-disable-line
};
