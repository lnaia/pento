import * as PropTypes from "prop-types";
import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useStyles } from "./loading.styles";

export const Loading = ({ className, size }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CircularProgress color="primary" className={className} size={size} />
    </div>
  );
};

Loading.propTypes = {
  className: PropTypes.string,
  size: PropTypes.number
};

Loading.defaultProps = {
  className: null,
  size: 50
};
