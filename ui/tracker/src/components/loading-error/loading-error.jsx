import * as PropTypes from "prop-types";
import React from "react";
import Button from "@material-ui/core/Button";
import { useStyles } from "./loading-error.styles";

export const LoadingError = ({ retry }) => {
  const classes = useStyles();
  return (
    <div className={classes.loadingError}>
      <div className={classes.title}>There has been a loading error.</div>
      <Button onClick={retry} href="" variant="contained" color="secondary">
        Please click to retry
      </Button>
    </div>
  );
};

LoadingError.propTypes = {
  retry: PropTypes.func.isRequired
};
