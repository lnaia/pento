import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  title: {
    marginBottom: '20px',
  },
  loadingError: {
    marginTop: '40px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
});
