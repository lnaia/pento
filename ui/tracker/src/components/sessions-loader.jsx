import React, { useEffect } from "react";
import * as PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { sessionsActions } from "actions/sessions";
import { ACTION_STATUS } from "constants/index";
import { Loading } from "components/loading/loading";
import { LoadingError } from "components/loading-error/loading-error";
import { selectSessionsRequestStatus } from "reducers/selectors";

export const SessionsLoader = ({ children }) => {
  const fetchStatus = useSelector(selectSessionsRequestStatus);
  const dispatch = useDispatch();
  const fetchList = () => dispatch(sessionsActions.start());

  useEffect(() => {
    fetchList();
  }, []);

  if (fetchStatus === ACTION_STATUS.SUCCESS) {
    return children;
  }
  if (fetchStatus === ACTION_STATUS.FAILURE) {
    return <LoadingError retry={fetchList} />;
  }

  return <Loading />;
};

SessionsLoader.propTypes = {
  children: PropTypes.node.isRequired
};
