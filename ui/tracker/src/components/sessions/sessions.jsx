import React from "react";
import { useSelector } from "react-redux";
import Paper from "@material-ui/core/Paper";
import { SessionsLoader } from "components/sessions-loader";
import { SessionHeader } from "./session-header";
import { SessionNew } from "./session-new";
import { SessionList } from "./session-list";
import { useStyles } from "./sessions.styles";
import { selectSessionsCreateSession } from "reducers/selectors/sessions";

export const Sessions = () => {
  const showCreateSession = useSelector(selectSessionsCreateSession);
  const classes = useStyles();
  return (
    <>
      <h1>Work Sessions</h1>
      <Paper className={classes.sessions}>
        <SessionHeader />
        {showCreateSession && <SessionNew />}
        <SessionsLoader>
          <SessionList />
        </SessionsLoader>
      </Paper>
    </>
  );
};
