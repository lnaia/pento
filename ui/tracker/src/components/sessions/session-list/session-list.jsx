import React from "react";
import * as PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import classnames from "classnames";
import {
  saveSessionActions,
  finishSessionActions,
  togglePlay
} from "actions/sessions";
import { SessionDetails } from "../session-details";
import { useStyles } from "./session-list.styles";
import { selectSessionsPayload } from "reducers/selectors/sessions";

export const SessionList = ({ className }) => {
  const listObj = useSelector(selectSessionsPayload);
  const dispatch = useDispatch();
  const onLock = payload => dispatch(finishSessionActions.start(payload));
  const onTogglePlay = payload => dispatch(togglePlay(payload));
  const onChangeName = payload => dispatch(saveSessionActions.start(payload));

  const classes = useStyles();
  const noSessions = Object.keys(listObj).length === 0;

  return (
    <div className={classnames(classes.root, className)}>
      {!noSessions && <h2>Work Sessions List</h2>}
      {noSessions && <p>There is no sessions yet, please try and add one!</p>}
      <>
        {Object.values(listObj).map(({ data: session }) => {
          const { id, name, state, totalTime, createdAt, startedAt } = session;
          return (
            <SessionDetails
              key={`${id}-${name}`}
              className={classes.session}
              id={id}
              name={name}
              state={state}
              totalTime={totalTime}
              createdAt={createdAt}
              startedAt={startedAt}
              onTogglePlay={onTogglePlay}
              onLock={onLock}
              onChangeName={onChangeName}
            />
          );
        })}
      </>
    </div>
  );
};

SessionList.propTypes = {
  className: PropTypes.string
};

SessionList.defaultProps = {
  className: null
};
