import React, { useState, useCallback } from "react";
import * as PropTypes from "prop-types";
import * as moment from "moment";
import { SESSION_STATE } from "constants/index";
import classnames from "classnames";
import TextField from "@material-ui/core/TextField";
import { SessionActions } from "../session-actions";
import { SessionDuration } from "../session-duration";
import { useStyles } from "./session-details.styles";

const { FINISHED } = SESSION_STATE;

const debounce = (fn, delay) => {
  let timeoutId;
  return function(...args) {
    clearInterval(timeoutId);
    timeoutId = setTimeout(() => fn.apply(this, args), delay);
  };
};

const DEBOUNCE_TIMER = 1000;

export const SessionDetails = ({
  id,
  name,
  totalTime,
  createdAt,
  startedAt,
  state,
  onTogglePlay,
  onLock,
  onChangeName,
  className
}) => {
  const classes = useStyles();
  const [input, setInput] = useState(name);
  const debounceCallback = useCallback(
    debounce(newName => {
      onChangeName({ id, name: newName });
    }, DEBOUNCE_TIMER),
    []
  );

  const handleOnChangeName = ({ target: { value } }) => {
    setInput(value);
    debounceCallback(value);
  };

  return (
    <div className={classnames(classes.root, className)}>
      <div className={classes.createdAt}>
        {createdAt.format("MMM Do YYYY, h:mm:ss a")}
      </div>

      <div className={classes.box}>
        <form noValidate autoComplete="off" className={classes.name}>
          <TextField
            defaultValue={input}
            label="Name"
            disabled={state === FINISHED}
            onChange={handleOnChangeName}
          />
        </form>

        <SessionActions
          state={state}
          onTogglePlay={() => onTogglePlay({ id, state })}
          onLock={() => onLock({ id })}
        />
        <div className={classes.totalTime}>
          <SessionDuration
            totalTime={totalTime}
            state={state}
            startedAt={startedAt}
          />
        </div>
      </div>
    </div>
  );
};

SessionDetails.propTypes = {
  ...SessionActions.propTypes,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  totalTime: PropTypes.number.isRequired,
  startedAt: PropTypes.instanceOf(moment).isRequired,
  createdAt: PropTypes.instanceOf(moment).isRequired,
  onChangeName: PropTypes.func.isRequired,
  className: PropTypes.string
};

SessionDetails.defaultProps = {
  className: null
};
