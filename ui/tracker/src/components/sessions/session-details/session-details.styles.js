import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
    backgroundColor: '#f9f9f9',
    borderRadius: 3,
    maxWidth: 'fit-content',
  },
  box: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 10,
  },
  createdAt: {
    fontSize: '.875rem',
    fontStyle: 'italic',
  },
  name: {
    marginRight: 15,
  },
  totalTime: {
    marginRight: 15,
  },
});
