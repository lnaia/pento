import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import { formatTime } from "reducers/data-transformers/duration";

export const StaticDuration = ({ totalTime, className }) => {
  return <span className={classnames(className)}>{formatTime(totalTime)}</span>;
};

StaticDuration.propTypes = {
  totalTime: PropTypes.number.isRequired,
  className: PropTypes.string
};

StaticDuration.defaultProps = {
  className: null
};
