import React, { useEffect, useState } from "react";
import * as PropTypes from "prop-types";
import * as moment from "moment";
import { StaticDuration } from "./static-duration";

export const DynamicDuration = ({ totalTime, startedAt, className }) => {
  const secondsSinceStarted = moment().diff(startedAt, "seconds");
  const [time, setTime] = useState(totalTime + secondsSinceStarted);

  useEffect(() => {
    const timerID = setInterval(() => setTime(time + 1), 1000);
    return () => clearInterval(timerID);
  });

  return <StaticDuration totalTime={time} className={className} />;
};

DynamicDuration.propTypes = {
  totalTime: PropTypes.number.isRequired,
  startedAt: PropTypes.instanceOf(moment).isRequired,
  className: PropTypes.string
};
DynamicDuration.defaultProps = {
  className: null
};
