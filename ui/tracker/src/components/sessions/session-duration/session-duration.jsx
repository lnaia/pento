import React from "react";
import * as PropTypes from "prop-types";
import * as moment from "moment";
import { SESSION_STATE } from "constants/index";
import { DynamicDuration } from "./dynamic-duration";
import { StaticDuration } from "./static-duration";

export const SessionDuration = ({ totalTime, startedAt, state, className }) => {
  if (state !== SESSION_STATE.STARTED) {
    return <StaticDuration totalTime={totalTime} className={className} />;
  }

  return (
    <DynamicDuration
      totalTime={totalTime}
      className={className}
      startedAt={startedAt}
    />
  );
};

SessionDuration.propTypes = {
  totalTime: PropTypes.number.isRequired,
  startedAt: PropTypes.instanceOf(moment).isRequired,
  state: PropTypes.string.isRequired,
  className: PropTypes.string
};

SessionDuration.defaultProps = {
  className: null
};
