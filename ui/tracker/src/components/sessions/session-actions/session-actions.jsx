import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import LockIcon from "@material-ui/icons/Lock";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import { SESSION_STATE } from "constants/sessions";
import IconButton from "@material-ui/core/IconButton";

const { INITIAL, STARTED, STOPPED, FINISHED } = SESSION_STATE;

export const SessionActions = ({ state, onTogglePlay, onLock, className }) => {
  const showPlayIcon =
    state === INITIAL || state === STOPPED || state === FINISHED;
  const showLockOpenIcon =
    state === INITIAL || state === STARTED || state === STOPPED;

  return (
    <div className={classnames(className)}>
      <IconButton onClick={onTogglePlay} disabled={state === FINISHED}>
        {showPlayIcon && (
          <PlayArrowIcon color={state === FINISHED ? undefined : "primary"} />
        )}
        {state === STARTED && <PauseIcon color="primary" />}
      </IconButton>

      <IconButton onClick={onLock} disabled={state === FINISHED}>
        {showLockOpenIcon && <LockOpenIcon color="secondary" />}
        {state === FINISHED && <LockIcon />}
      </IconButton>
    </div>
  );
};

SessionActions.propTypes = {
  className: PropTypes.string,
  state: PropTypes.string.isRequired,
  onTogglePlay: PropTypes.func.isRequired,
  onLock: PropTypes.func.isRequired
};

SessionActions.defaultProps = {
  className: null
};
