import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  addNewSession: {
    marginRight: 30,
  },
  root: {
    display: 'flex',
  },
  formControl: {
    width: '80%',
  },
});
