import React from "react";
import * as PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import classnames from "classnames";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";
import { changeListOverview, showCreateSession } from "actions/sessions";
import { useStyles } from "./session-header.styles";
import { selectSessionsOverview } from "reducers/selectors/sessions";

export const SessionHeader = ({ className }) => {
  const classes = useStyles();
  const overview = useSelector(selectSessionsOverview);
  const dispatch = useDispatch();

  const onClickCreate = () => dispatch(showCreateSession);
  const handleChange = overview => dispatch(changeListOverview({ overview }));

  return (
    <div className={classnames(classes.root, className)}>
      <Fab
        color="primary"
        onClick={onClickCreate}
        aria-label="add"
        className={classes.addNewSession}
      >
        <AddIcon />
      </Fab>

      <FormControl className={classes.formControl}>
        <Select
          value={overview}
          onChange={e => handleChange(e.target.value)}
          displayEmpty
          className={classes.select}
        >
          <MenuItem value="all">All</MenuItem>
          <MenuItem value="day">Today&apos;s</MenuItem>
          <MenuItem value="week">This week</MenuItem>
          <MenuItem value="month">This month</MenuItem>
        </Select>
        <FormHelperText>Session filter</FormHelperText>
      </FormControl>
    </div>
  );
};
SessionHeader.propTypes = {
  className: PropTypes.string
};

SessionHeader.defaultProps = {
  className: null
};
