/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { SESSION_STATE } from 'constants/index';
import { SessionActions } from '../session-actions';

const ref = storiesOf('Session / Actions', module);

Object.values(SESSION_STATE)
  .forEach((state) => ref.add(state, () => (
    <SessionActions
      onLock={action('onLock')}
      onTogglePlay={action('onTogglePlay')}
      state={state}
    />
  )));
