/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { SessionNew } from '../session-new';

storiesOf('Session / New', module)
  .add('SessionNew', () => (
    <SessionNew
      onSave={action('onSave')}
      onClose={action('onClose')}
    />
  ));
