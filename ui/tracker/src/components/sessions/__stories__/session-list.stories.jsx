/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { sessionMapper } from "reducers/data-transformers";
import { SessionList } from "../session-list/session-list";
import { sessions } from "../__mocks__/session.mock";

storiesOf("Session / List", module).add("SessionList", () => (
  <SessionList
    onLock={action("onLock")}
    onChangeName={action("onChangeName")}
    onTogglePlay={action("onTogglePlay")}
    list={sessions.map(sessionMapper)}
  />
));
