/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { SESSION_STATE } from "constants/index";
import { sessionMapper } from "reducers/data-transformers";
import { SessionDetails } from "../session-details";
import { session } from "../__mocks__/session.mock";

const ref = storiesOf("Session / Details", module);

Object.values(SESSION_STATE).forEach(state =>
  ref.add(`Details - ${state}`, () => (
    <SessionDetails
      onLock={action("onLock")}
      onChangeName={action("onChangeName")}
      onTogglePlay={action("onTogglePlay")}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...sessionMapper(session)}
      state={state}
    />
  ))
);
