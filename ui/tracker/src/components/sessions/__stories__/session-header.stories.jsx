/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { SessionHeader } from '../session-header';

storiesOf('Session / Header', module)
  .add('Header - today', () => (
    <SessionHeader
      overview="all"
      onClickCreate={action('onClickCreate')}
      onChangeOverview={action('onChangeOverview')}
    />
  ));
