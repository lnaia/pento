import React, { useState } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import { useDispatch } from "react-redux";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Close";
import { createSessionActions, hideCreateSession } from "actions/sessions";
import { useStyles } from "./session-new.styles";

export const SessionNew = ({ className }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const onSave = payload => dispatch(createSessionActions.start(payload));
  const onClose = () => dispatch(hideCreateSession);
  const [name, setName] = useState("");

  const handleSave = () => onSave({ name });
  const handleKeyPress = e => {
    if (e.key === "Enter") {
      handleSave();
      e.preventDefault();
    }
  };

  return (
    <Paper component="form" className={classnames(classes.root, className)}>
      <InputBase
        onKeyPress={handleKeyPress}
        className={classes.input}
        placeholder="Session name"
        inputProps={{ "aria-label": "session name" }}
        onChange={e => setName(e.target.value)}
      />
      <IconButton
        type="button"
        onClick={handleSave}
        className={classes.iconButton}
        aria-label="name"
      >
        <SaveIcon />
      </IconButton>
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton
        color="primary"
        onClick={onClose}
        className={classes.iconButton}
        aria-label="cancel"
      >
        <CloseIcon />
      </IconButton>
    </Paper>
  );
};

SessionNew.propTypes = {
  className: PropTypes.string
};

SessionNew.defaultProps = {
  className: null
};
