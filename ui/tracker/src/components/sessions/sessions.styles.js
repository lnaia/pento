import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  sessions: {
    padding: 40,
    minWidth: 400,
  },
});
