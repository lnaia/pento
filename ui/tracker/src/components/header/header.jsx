import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import GitHubIcon from "@material-ui/icons/GitHub";
import { useStyles } from "./header.styles";

export const Header = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="fixed" color="primary">
        <Toolbar>
          <div className={classes.title}>
            <h2>Time Tracker</h2>
          </div>

          <IconButton
            href="https://gitlab.com/lnaia/pento/"
            target="_blank  "
            color="inherit"
            className={classes.btnSettings}
          >
            <GitHubIcon className={classes.btnIco} />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
};
