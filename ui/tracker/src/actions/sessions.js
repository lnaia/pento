import { ACTIONS } from 'constants/actions';
import { SESSION_STATE } from 'constants/sessions';
import { createGenericApiAction } from 'utils';

const {
  SAVE_SESSION,
  STOP_SESSION,
  FETCH_SESSION,
  START_SESSION,
  FETCH_SESSIONS,
  CREATE_SESSION,
  FINISH_SESSION,
  SHOW_CREATE_SESSION,
  HIDE_CREATE_SESSION,
  ON_CHANGE_LIST_OVERVIEW,
} = ACTIONS;

export const sessionActions = createGenericApiAction(FETCH_SESSION);
export const sessionsActions = createGenericApiAction(FETCH_SESSIONS);
export const createSessionActions = createGenericApiAction(CREATE_SESSION);
export const saveSessionActions = createGenericApiAction(SAVE_SESSION);
export const startSessionActions = createGenericApiAction(START_SESSION);
export const stopSessionActions = createGenericApiAction(STOP_SESSION);
export const finishSessionActions = createGenericApiAction(FINISH_SESSION);

export const showCreateSession = { type: SHOW_CREATE_SESSION };
export const hideCreateSession = { type: HIDE_CREATE_SESSION };

export const togglePlay = (data) => {
  const { state } = data;
  return state === SESSION_STATE.STARTED
    ? stopSessionActions.start(data)
    : startSessionActions.start(data);
};

export const changeListOverview = ({ overview: payload }) => ({
  type: ON_CHANGE_LIST_OVERVIEW,
  payload,
});
