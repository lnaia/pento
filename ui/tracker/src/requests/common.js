export const generatePayload = ({
  cache = undefined,
  data = undefined,
  method = 'GET',
} = {}) => {
  const payload = { method: method.toUpperCase() };
  const headers = {};

  if (`${method}`.toUpperCase() !== 'GET') {
    headers['Content-Type'] = 'application/json; charset=utf-8';
  }

  if (data) {
    payload.body = JSON.stringify(data);
  }

  if (cache) {
    payload.cache = cache;
  }

  return { ...payload, headers: new Headers(headers) };
};

export const generateRequest = async ({
  url,
  method = undefined,
  data = undefined,
  cache = undefined,
} = {}) => {
  const response = await fetch(url, generatePayload({ data, method, cache }));
  if (!response.ok) {
    throw response.status;
  }

  const text = await response.text();
  return text.length ? JSON.parse(text) : {};
};

export const get = (url, { data = undefined, cache = undefined } = {}) =>
  generateRequest({ url, data, cache });

export const post = (url, { data = undefined } = {}) =>
  generateRequest({ url, data, method: 'post' });

export const put = (url, { data = undefined } = {}) =>
  generateRequest({ url, data, method: 'put' });
