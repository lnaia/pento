import { SESSION_OVERVIEW } from 'constants/index';
import { get, post, put } from './common';

export const listSessions = (apiUrl, { overview, perPage }) => {
  const params = { per_page: perPage };
  if (overview !== SESSION_OVERVIEW.ALL) {
    params.overview = overview;
  }
  const query = new URLSearchParams(params).toString();
  return get(`${apiUrl}/sessions?${query}`);
};

export const singleSessionRequest = (apiUrl, params) =>
  get(`${apiUrl}/sessions/${params.id}`);

export const createSessionRequest = (apiUrl, data) =>
  post(`${apiUrl}/sessions`, { data });

export const saveSessionRequest = (apiUrl, { id, data }) =>
  put(`${apiUrl}/sessions/${id}`, { data });

export const sessionActionRequest = (apiUrl, { id }, actionType) =>
  post(`${apiUrl}/sessions/${id}/actions?action_type=${actionType}`);

export const startSessionActionRequest = (...args) =>
  sessionActionRequest(...args, 'start');

export const stopSessionActionRequest = (...args) =>
  sessionActionRequest(...args, 'stop');

export const finishSessionActionRequest = (...args) =>
  sessionActionRequest(...args, 'finish');
