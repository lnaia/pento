import { put, call, select } from 'redux-saga/effects';
import { ACTION_STATUS } from 'constants/action-status';
import { selectApiEndpoint } from 'reducers/selectors/app';

const {
  IN_PROGRESS, SUCCESS, FAILURE, NOT_STARTED,
} = ACTION_STATUS;

export const bindActionPrefix = (prefix, sufix) => `${prefix}_${sufix}`;

export const createGenericApiAction = (type) => {
  const aux = (sufix) => (payload, meta) => ({
    type: bindActionPrefix(type, sufix),
    payload,
    meta,
  });

  return {
    start: (payload) => ({ type, payload }),
    progress: aux(IN_PROGRESS),
    success: aux(SUCCESS),
    failure: aux(FAILURE),
  };
};

export const genericApiReducerInitialState = {
  payload: null,
  meta: {
    requestStatus: NOT_STARTED,
  },
};

export const createGenericApiReducer = (
  prefix,
  dataTransformer = (payload) => payload,
) => (state = genericApiReducerInitialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case `${bindActionPrefix(prefix, IN_PROGRESS)}`: {
      return {
        ...state,
        meta: {
          ...state.meta,
          requestStatus: IN_PROGRESS,
        },
      };
    }

    case `${bindActionPrefix(prefix, SUCCESS)}`: {
      return {
        ...state,
        payload: dataTransformer(payload),
        meta: {
          ...state.meta,
          requestStatus: SUCCESS,
        },
      };
    }

    case `${bindActionPrefix(prefix, FAILURE)}`: {
      return {
        ...state,
        payload: dataTransformer(payload),
        meta: {
          ...state.meta,
          requestStatus: FAILURE,
        },
      };
    }

    default:
      return state;
  }
};

export function* genericSaga({
  progress, success, failure, request, params,
}) {
  yield put(progress(params));

  const apiUrl = yield select(selectApiEndpoint);

  try {
    const response = yield call(request, apiUrl, params);
    if (response) {
      yield put(success(response, { params }));
    } else {
      yield put(failure(params));
    }
  } catch (err) {
    // eslint-disable-next-line
    console.error(err); // TODO: send to logger
    yield put(failure(err));
  }
}
