import { applyMiddleware, compose, createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { rootReducer } from 'reducers';
import { rootSaga } from 'sagas';
import { loadSavedState } from './saved-state';
import { registerSubscribers } from './subscribers';

export const configure = () => {
  const sagaMiddleware = createSagaMiddleware();
  const initialState = loadSavedState();
  const middleware = [applyMiddleware(sagaMiddleware)];

  // eslint-disable-next-line no-underscore-dangle
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    middleware.push(devToolsEnhancer());
  }

  const store = createStore(rootReducer, initialState, compose(...middleware));

  sagaMiddleware.run(rootSaga);
  registerSubscribers(store);
  return store;
};
