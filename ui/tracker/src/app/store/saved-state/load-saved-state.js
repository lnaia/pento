import { get } from 'lodash';
import { metaInitialState } from 'reducers/sessions/meta';

const NO_LOCAL_STORAGE_DATA = -1;

export const getData = (key) => {
  const rawData = get(window, `localStorage.${key}`, NO_LOCAL_STORAGE_DATA);
  if (rawData !== NO_LOCAL_STORAGE_DATA) {
    return JSON.parse(rawData).data;
  }
  return NO_LOCAL_STORAGE_DATA;
};

export const loadSavedState = () => {
  const savedStates = [];
  const initialState = {
    app: { apiEndpoint: process.env.REACT_APP_API_ENDPOINT },
  };

  const meta = getData('sessions.meta');
  if (meta !== NO_LOCAL_STORAGE_DATA) {
    initialState.sessions = {
      ...metaInitialState,
      meta,
    };
  }

  return savedStates.reduce(
    (acc, curr) => Object.assign(acc, curr),
    initialState,
  );
};
