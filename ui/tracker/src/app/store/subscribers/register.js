import { sessionsMeta } from './sessions';

export const subscriberList = () => [sessionsMeta];

export const registerSubscribers = (store) =>
  subscriberList().forEach((subscriber) => store.subscribe(subscriber(store)));
