import { selectSessionsMeta } from 'reducers/selectors/sessions';

export const sessionsMeta = (store) => () => {
  try {
    const state = store.getState();
    const { localStorage } = window;
    const data = selectSessionsMeta(state);
    localStorage.setItem('sessions.meta', JSON.stringify({ data }));
  } catch (err) {
    // ignore write errors
  }
};
