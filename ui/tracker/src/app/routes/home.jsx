import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { HomeContainer } from 'containers';
import { DefaultLayoutComponent } from 'components';

export const HomeRoute = () => (
  <Router>
    <DefaultLayoutComponent exact path="/" component={HomeContainer} />
  </Router>
);
