import React from "react";
import { Provider } from "react-redux";
import { configure } from "./store";
import { HomeRoute } from "./routes";
import "./fonts.css";
import "./index.css";

export const App = () => {
  return (
    <Provider store={configure()}>
      <HomeRoute />
    </Provider>
  );
};
