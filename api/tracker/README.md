# Time Tracker - API

This api was developed w/ [Buffalo](http://gobuffalo.io).

## Dev Requirements

- goland: go1.13.4
- gobuffalo: v0.15.2
- postman

## Local development

- start the database
- db: create the database
- db: run migrations
- start the application
- generate seed data
- run tests

### Start the database

At the root of the repository run:
	$ docker-compose up tracker_db

### Create the db and run migrations

	$ buffalo pop create
	$ buffalo pop migrate

### Starting the Application

	$ buffalo dev

### Generate seed data

	$ buffalo tasks db_seed

### Run tests

	$ buffalo test


## Session State

![Session States](documentation/session_states.png "Session Possible States")

[Link to editor](https://mermaidjs.github.io/mermaid-live-editor/#/view/eyJjb2RlIjoiZ3JhcGggVERcbkFbaW5pdGlhbF0gLS0-fHN0YXJ0fCBCXG5CW3N0YXJ0ZWRdIC0tPiB8c3RhcnR8QlxuQiAtLT4gfGZpbmlzaHwgRFxuQiAtLT4gfHN0b3B8IENcbkNbc3RvcHBlZF0gLS0-fHN0YXJ0fCBCXG5DIC0tPnxzdG9wfCBDXG5DIC0tPnxmaW5pc2h8IERcbkRbZmluaXNoZWRdIC0tPnxmaW5pc2h8IERcbiIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In19)

## Api documentation

- [Postman: Online Documentation](https://documenter.getpostman.com/view/968908/SWDze1CT?version=latest#38e7334d-5947-4a70-b510-a89ab10a2496)
- [Postman: Collection](documentation/time-tracker.postman_collection.json)
