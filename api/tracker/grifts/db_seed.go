package grifts

import (
	"fmt"
	"math/rand"
	"time"
	"tracker/models"
	"tracker/services"

	. "github.com/markbates/grift/grift"
)

var _ = Desc("db_seed", "Seeds the development database")
var _ = Add("db_seed", func(c *Context) error {
	return generateSeedData()
})

func generateSeedData() error {
	var states []string
	states = append(states, services.SessionStateReady)
	states = append(states, services.SessionStateStop)
	states = append(states, services.SessionStateFinish)

	for month := 1; month <= 12; month++ {
		for day := 1; day <= 25; day++ {

			maxSessions := rand.Intn(9)
			for s := 1; s <= maxSessions; s++ {
				state := states[rand.Intn(3)]
				target := fmt.Sprintf("2019-%02d-%02dT%02d:00:00+00:00", month, day, s)
				t, _ := time.Parse(time.RFC3339, target)

				// we don't want tasks set in the future
				today := time.Now()
				if t.After(today) {
					return nil
				}

				totalTaskTime := rand.Intn(3600)
				createSession(t, state, totalTaskTime)
			}
		}
	}

	return nil
}

func createSession(createdAt time.Time, currentState string, totalTaskTime int) {
	session := models.Session{
		Name:         RandStringBytesMaskImpr(8),
		CurrentState: currentState,
		TotalTime:    totalTaskTime,
		CreatedAt:    createdAt,
	}

	db := models.DB
	_, err := db.ValidateAndSave(&session)
	if err != nil {
		services.Log.Fatal(err)
	}

	services.Log.Infof("session created: %s, ideally: %s", session.CreatedAt, createdAt)
}

// random strings
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandStringBytesMaskImpr(n int) string {
	b := make([]byte, n)
	// A rand.Int63() generates 63 random bits, enough for letterIdxMax letters!
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
