package actions

import (
	"fmt"
	"tracker/models"
)

func (as *ActionSuite) Test_SessionsResource_List() {
	as.LoadFixture("two sessions")
	res := as.JSON("/sessions").Get()
	as.Equal(200, res.Code)
	body := res.Body.String()
	as.Contains(body, "session1")
	as.Contains(body, "session2")
}

func (as *ActionSuite) Test_SessionsResource_Show() {
	as.LoadFixture("two sessions")
	res := as.JSON("/sessions").Get()
	as.Equal(200, res.Code)
	body := res.Body.String()
	as.Contains(body, "7cb793ec-dd69-4c1d-89f6-ac3fa4123112")
}

func (as *ActionSuite) Test_SessionsResource_Create() {
	name := "MySession"
	w := &models.Session{Name: name}
	res := as.JSON("/sessions").Post(w)
	as.Equal(201, res.Code)

	err := as.DB.Last(w)
	as.NoError(err)
	as.NotZero(w.ID)
	as.Equal(name, w.Name)
}

func (as *ActionSuite) Test_SessionsResource_Update() {
	as.LoadFixture("two sessions")
	name := "NewSessionName"
	last := &models.Session{}
	err := as.DB.Last(last)
	as.NoError(err)
	as.NotZero(last.ID)

	last.Name = name

	url := fmt.Sprintf("/sessions/%s", last.ID)
	res := as.JSON(url).Put(last)
	as.Equal(200, res.Code)

	w := &models.Session{}
	err = as.DB.Last(w)
	as.NoError(err)
	as.Equal(name, w.Name)
	as.Equal(w.ID, last.ID)
}
