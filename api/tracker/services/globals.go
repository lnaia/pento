package services

import (
	"github.com/gobuffalo/logger"
)

// Log logger service interface
var Log = logger.NewLogger("debug")

// SessionStateReady initial session state
const SessionStateReady = "initialState"

// SessionStateStart start state
const SessionStateStart = "started"

// SessionStateStop stop state
const SessionStateStop = "stopped"

// SessionStateFinish final state
const SessionStateFinish = "finished"

// MaxUint max unsigned int
const MaxUint = ^uint(0)

// MaxInt max int, inferred
const MaxInt = int(MaxUint >> 1)
