package services

import (
	"errors"
	"time"
	"tracker/models"
)

// HandleActions determines which action is to run on the session
func HandleActions(actionType string, session *models.Session) error {
	// invalid action - notify requester
	targetState, err := parseTargetState(actionType)
	if err != nil {
		return err
	}

	// invalid state transition - no need to notify
	valid := validateState(session.CurrentState, targetState)
	if !valid {
		Log.Infof("invalid transition from %s to %s", session.CurrentState, targetState)
		return nil
	}

	if targetState == SessionStateStart {
		// start state can be reached from: ready, start, stop
		return startTracking(session)

	} else if targetState == SessionStateStop {
		// stop state can be reached from: start, stop
		return stopTracking(session)

	} else if targetState == SessionStateFinish {
		// finish state can be reached from: start, stop, finish
		if session.CurrentState == SessionStateStart {
			err = stopTracking(session)
			if err != nil {
				return err
			}
		}
		return finishTracking(session)
	}

	return nil
}

// startTracking logic for start tracking sessions
func startTracking(session *models.Session) error {
	Log.Debugf("startTracking: %s", session.ID)
	if session.CurrentState == SessionStateStart {
		return nil
	}

	db := models.DB
	session.StartedAt = time.Now()
	session.CurrentState = SessionStateStart
	_, err := db.ValidateAndUpdate(session)
	if err != nil {
		return err
	}

	return nil
}

// stopTracking logic for start stop sessions
func stopTracking(session *models.Session) error {
	Log.Debugf("stopTracking: %s", session.ID)
	if session.CurrentState == SessionStateStop {
		return nil
	}

	db := models.DB
	secondsDiff := time.Now().Sub(session.StartedAt).Seconds()
	session.TotalTime += int(secondsDiff) // ok if overflow
	session.CurrentState = SessionStateStop

	_, err := db.ValidateAndUpdate(session)
	if err != nil {
		return err
	}

	return nil
}

// finishTrackings logic for start finish sessions
func finishTracking(session *models.Session) error {
	Log.Debugf("finishTracking: %s", session.ID)
	if session.CurrentState == SessionStateFinish {
		return nil
	}

	db := models.DB
	session.CurrentState = SessionStateFinish

	// there's no going back once the task is saved
	_, err := db.ValidateAndUpdate(session)
	if err != nil {
		return err
	}

	return nil
}

// parseTargetState parses a action_type string to determine a valid session state
func parseTargetState(actionType string) (string, error) {
	if actionType == "start" {
		return SessionStateStart, nil

	} else if actionType == "stop" {
		return SessionStateStop, nil

	} else if actionType == "finish" {
		return SessionStateFinish, nil
	}

	return SessionStateReady, errors.New("action type not found")
}

// validateState validates allowed state transitions
// states: ready, start, stop, finish
// initial state: ready
// final state: finish
func validateState(currentState string, targetState string) bool {
	if currentState == targetState {
		return true
	}

	// ready goes to: start
	if currentState == SessionStateReady && targetState == SessionStateStart {
		return true
	}

	// start goes to: start, stop, finish
	if currentState == SessionStateStart && targetState != SessionStateReady {
		return true
	}

	// stop goes to: start, stop, finish
	if currentState == SessionStateStop && targetState != SessionStateReady {
		return true
	}

	// finish goes to: finish
	if currentState == SessionStateFinish && targetState == SessionStateFinish {
		return true
	}

	return false
}
